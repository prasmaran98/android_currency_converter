package com.example.troopers_currency_converter.model.network.convert

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Query(
    val amount: Double,
    val from: String,
    val to: String
) : Parcelable