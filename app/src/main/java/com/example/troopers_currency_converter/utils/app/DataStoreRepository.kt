package com.example.troopers_currency_converter.utils.app

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException
import javax.inject.Inject

// At the top level of your kotlin file:
private val Context.dataStore : DataStore<androidx.datastore.preferences.core.Preferences> by preferencesDataStore(name = "settings")

@ViewModelScoped
class DataStoreRepository @Inject constructor(@ApplicationContext private val context: Context) {

    //private val Context.dataStore : DataStore<androidx.datastore.preferences.core.Preferences> by preferencesDataStore(name = "settings")
    private val dataStore = context.dataStore

    private object PreferenceKeys {
        val userBaseCurrency = stringPreferencesKey("USD")
    }

    // Functions to save the preferences
    suspend fun saveUserBaseCurrency(baseCurrency: String) {
        dataStore.edit { preference ->
            preference[PreferenceKeys.userBaseCurrency] = baseCurrency
        }
    }

    val readUserBaseCurrency = dataStore.data
        .catch { exception ->
            if (exception is IOException) {
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }
        .map { preferences ->
            val userBaseCurrency = preferences[PreferenceKeys.userBaseCurrency] ?: "JPY"
            UserBaseCurrency(userBaseCurrency)
        }

}

data class UserBaseCurrency(val baseCurrency: String)
