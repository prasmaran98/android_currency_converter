package com.example.troopers_currency_converter.model.network.convert

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Info(
    val rate: Double,
    val timestamp: Int
) : Parcelable