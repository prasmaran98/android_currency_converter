package com.example.troopers_currency_converter.utils.constant

class Constants {

    companion object{

        const val BASE_URL = "https://api.apilayer.com/"

        // Database
        const val CURRENCY_DATABASE = "currencyRateDatabase"
        const val CURRENCY_TABLE = "currency_rate_table"

        // Error
        const val NETWORK_ERROR_NO_INTERNET = "Network Error"

        // Preferences
        const val PREFERENCES_NAME = "my_settings"

    }
}