package com.example.troopers_currency_converter.model.network

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CurrencyRate(
    val symbol: String,
    val rate: Double
) : Parcelable
