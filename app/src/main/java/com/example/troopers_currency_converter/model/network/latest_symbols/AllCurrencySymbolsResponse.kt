package com.example.troopers_currency_converter.model.network.latest_symbols

data class AllCurrencySymbolsResponse(
    val success: Boolean,
    val symbols: Symbols
)