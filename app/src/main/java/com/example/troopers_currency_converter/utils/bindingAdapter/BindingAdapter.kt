package com.example.troopers_currency_converter.utils.bindingAdapter

import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.navigation.findNavController
import coil.load
import com.example.troopers_currency_converter.R
import com.example.troopers_currency_converter.model.Currency
import com.example.troopers_currency_converter.model.network.CurrencyRate
import com.example.troopers_currency_converter.ui.fragment.currencyList.CurrencyListFragmentDirections

class BindingAdapter {

    companion object {

        /** Currency List Binding Adapters **/

        @BindingAdapter("bindCurrencyRateValue")
        @JvmStatic
        fun bindLatestCurrencyRateValue(textView: TextView, result: CurrencyRate){
            val currencyRateWithSign = "${Currency.valueOf(result.symbol).symbol} ${result.rate}"
            textView.text = currencyRateWithSign
        }

        @BindingAdapter("bindCurrencyFlag")
        @JvmStatic
        fun bindCurrencyFlag(imageView: ImageView, result: CurrencyRate){
            val currencyFlag: Int? = Currency.valueOf(result.symbol).flag
            if (currencyFlag != null) {
                imageView.load(currencyFlag) {
                    error(currencyFlag)
                }
            } else {
                imageView.load(R.drawable.flag_unknown)
            }
        }

        @BindingAdapter("bindCurrencyFullName")
        @JvmStatic
        fun bindCurrencyFullName(textView: TextView, result: CurrencyRate){
            val fullDetails = "${result.symbol} -- ${Currency.valueOf(result.symbol).fullName}"
            textView.text = fullDetails

        }

        /** Currency Chart Binding Adapters **/

        @BindingAdapter("navigateToChartsFragment")
        @JvmStatic
        fun navigateToChartsFragment(
            currencyItemLayout: ConstraintLayout,
            currencyName: CurrencyRate
        ){
            currencyItemLayout.setOnClickListener {
                try {
                    val action = CurrencyListFragmentDirections.actionCurrencyListFragmentToChartFragment(currencyName)
                    it.findNavController().navigate(action)
                } catch (e: Exception) {
                    Log.e("TAG-navigateChart", e.toString())
                }
            }
        }




//        val currencyShort =  Currency.valueOf(currencyDataItem.symbol).short
//        val currencyFlag: Int = Currency.valueOf(currencyShort).flag!!
//        val currencySign = Currency.valueOf(currencyDataItem.symbol).symbol // Currency Rate symbol refers to short hand, mistakenly named the data class val
//        val currencyFullName = Currency.valueOf(currencyDataItem.symbol).fullName
//
//        binding.currencyFlagImage.setImageResource(currencyFlag)
//        val currencyRateToBind = "$currencySign ${currencyDataItem.rate.roundToInt()}"
//        val currencyDetailsToBind = "${currencyDataItem.symbol} -- $currencyFullName"
//        binding.rateTv = currencyRateToBind
//        binding.currencyFullNameTv = currencyDetailsToBind


    }

}