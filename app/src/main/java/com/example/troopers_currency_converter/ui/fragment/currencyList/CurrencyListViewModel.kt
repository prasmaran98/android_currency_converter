package com.example.troopers_currency_converter.ui.fragment.currencyList

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.example.troopers_currency_converter.model.network.convert.ConvertAmountFromToResponse
import com.example.troopers_currency_converter.model.network.latest_rates.LatestRateResponse
import com.example.troopers_currency_converter.utils.app.DataStoreRepository
import com.example.troopers_currency_converter.utils.database.CurrencyEntity
import com.example.troopers_currency_converter.utils.network.resource.NetworkResult
import com.example.troopers_currency_converter.utils.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class CurrencyListViewModel @Inject constructor(
    application: Application,
    private val repository: Repository,
    private val dataStoreRepository: DataStoreRepository
) : AndroidViewModel(application) {

    //TODO: Network Listener
    var networkStatus: Boolean = false
    var backOnline: Boolean = false

    private val _latestRateResponse = MutableLiveData<NetworkResult<LatestRateResponse>>()
    var latestRateResponse: LiveData<NetworkResult<LatestRateResponse>> = _latestRateResponse

    val readUserBaseCurrency = dataStoreRepository.readUserBaseCurrency


    /** Room Database **/
    var readDataBase: LiveData<List<CurrencyEntity>> =
        repository.local.readDataBase().asLiveData()

    private fun insertCurrencyRate(currencyEntity: CurrencyEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.local.insertCurrencyRate(currencyEntity)
        }
    }

    /** API Calls **/
    fun getLatestRate(
        base: String
    ) = viewModelScope.launch {
        getLatestRateSafeCall(base)
    }

    private suspend fun getLatestRateSafeCall(
        base: String
    ) {
        _latestRateResponse.value = NetworkResult.Loading()

        try {
            val response = repository.remote.getLatestRate(base)
            _latestRateResponse.value = handleGetLatestRateResponse(response)

            //TODO: Offline caching
            val currencyRates = _latestRateResponse.value!!.data
            if (currencyRates != null) {
                offlineCurrencyRateCache(currencyRates)
            }

        } catch (e: Exception) {
            _latestRateResponse.value = NetworkResult.Error(e.message.toString())
            Log.e("CurrencyViewModelTag", "getLatestRateSafeCall: Error: ${e.message.toString()}")
        }
    }

    private fun handleGetLatestRateResponse(response: Response<LatestRateResponse>): NetworkResult<LatestRateResponse>? {

        return when {
            !response.body()!!.success -> NetworkResult.Error("An error occurred")
            response.isSuccessful -> {
                val data = response.body()
                NetworkResult.Success(data!!)
            }
            else -> {
                NetworkResult.Error(response.message())
            }
        }
    }

    private fun offlineCurrencyRateCache(latestRateResponse: LatestRateResponse) {
        val currencyEntity = CurrencyEntity(latestRateResponse)
        insertCurrencyRate(currencyEntity)
    }

}