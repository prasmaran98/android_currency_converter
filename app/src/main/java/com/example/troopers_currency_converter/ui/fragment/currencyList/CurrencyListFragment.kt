package com.example.troopers_currency_converter.ui.fragment.currencyList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.troopers_currency_converter.R
import com.example.troopers_currency_converter.databinding.FragmentCurrencyListBinding
import com.example.troopers_currency_converter.model.Currency
import com.example.troopers_currency_converter.model.network.CurrencyRate
import com.example.troopers_currency_converter.utils.adapter.rvAdapter.CurrencyListAdapter
import com.example.troopers_currency_converter.utils.app.AppUtils
import com.example.troopers_currency_converter.utils.network.resource.NetworkResult
import kotlinx.coroutines.launch

class CurrencyListFragment : Fragment() {

    private var _binding: FragmentCurrencyListBinding? = null
    private val binding get() = _binding!!

    private lateinit var currencyListViewModel: CurrencyListViewModel
    private val mAdapter by lazy { CurrencyListAdapter() }
    private var currencyRateList = mutableListOf<CurrencyRate>()
    private lateinit var userBaseCurrency: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currencyListViewModel =
            ViewModelProvider(requireActivity())[CurrencyListViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentCurrencyListBinding.inflate(inflater, container, false)
        val view = binding.root

        readCachedUserBaseCurrency()
        setupRecyclerView()

        println("BASE CURRENCY --> $userBaseCurrency")

        binding.currencyListBaseCurrencyCV.setOnClickListener {
            callCurrencyRateApi()
        }

        (activity as AppCompatActivity).supportActionBar?.hide()
        binding.homeFragToolbar.toolbarTitle.apply {
            text = getString(R.string.currency_list)
        }

        readDataBase()

        return view
    }

    private fun readCachedUserBaseCurrency() {
        userBaseCurrency = "JPY"
        viewLifecycleOwner.lifecycleScope.launch {
            currencyListViewModel.readUserBaseCurrency.collect { values ->
                userBaseCurrency = values.baseCurrency
            }
        }
        setBaseCurrencyView()
    }

    private fun setBaseCurrencyView() {
        val baseCurrencyFlag: Int = Currency.valueOf(userBaseCurrency).flag!!
        binding.apply {
            this.baseCurrencyFlagIV.setImageResource(baseCurrencyFlag)
            this.chosenBaseCurrencyTv.text = userBaseCurrency
            binding.baseCurrencyDetail.text = getString(R.string.click_to_fetch)
        }
    }

    private fun readDataBase() {
        viewLifecycleOwner.lifecycleScope.launch {
            currencyListViewModel.readDataBase.observe(viewLifecycleOwner) { database ->
                if (database.isNotEmpty()) {
                    currencyRateList.clear()
                    database[0].currencyRate.rates.toSortedMap().forEach { (symbol, rate) ->
                        currencyRateList.add(
                            CurrencyRate(
                                symbol,
                                rate
                            )
                        )
                    }
                    mAdapter.setData(currencyRateList)
                } else {
                    mAdapter.setData(emptyList())
                    binding.apply {
                        this.noCurrencyRateTv.visibility = View.VISIBLE
                        this.noCurrencyRateImageView.visibility = View.VISIBLE
                    }
                }
            }
            hideShimmerEffect()
        }
    }

    private fun callCurrencyRateApi() {
        showShimmerEffect()
        currencyListViewModel.getLatestRate(userBaseCurrency)
        currencyListViewModel.latestRateResponse.observe(viewLifecycleOwner) { response ->

            //binding.currencyListProgressBar.visibility = View.VISIBLE
            when (response) {

                is NetworkResult.Success -> {
                    binding.apply {
                        this.noCurrencyRateTv.visibility = View.GONE
                        this.noCurrencyRateImageView.visibility = View.GONE
                    }
                    hideShimmerEffect()
                    //binding.currencyListProgressBar.visibility = View.GONE
                    val successData = response.data!!.rates.toSortedMap()

                    successData.forEach { (symbol, rate) ->
                        currencyRateList.add(
                            CurrencyRate(
                                symbol,
                                rate
                            )
                        )
                    }
                    AppUtils.showToast(requireContext(), "Successfully retrieved")
                    mAdapter.setData(currencyRateList)
                }

                is NetworkResult.Error -> {
                    hideShimmerEffect()
                    //binding.currencyListProgressBar.visibility = View.GONE
                    AppUtils.showToast(requireContext(), response.message.toString())
                }

                is NetworkResult.Loading -> {
                    hideShimmerEffect()
                    //binding.currencyListProgressBar.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun setupRecyclerView() {
        binding.currencyListRecyclerView.adapter = mAdapter
        binding.currencyListRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        showShimmerEffect()
    }

    private fun showShimmerEffect() {
        binding.currencyListRecyclerView.showShimmer()
    }

    private fun hideShimmerEffect() {
        binding.currencyListRecyclerView.hideShimmer()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}