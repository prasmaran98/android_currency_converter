package com.example.troopers_currency_converter.ui.activity.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.troopers_currency_converter.R
import com.example.troopers_currency_converter.databinding.ActivityMainBinding
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_Troopers_Currency_Converter)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navView: BottomNavigationView = binding.navView

        navController = findNavController(R.id.nav_host_fragment_activity_main)

        /** Passing each menu ID as a set of Ids because each
            menu should be considered as top level destinations
        **/
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.converterFragment,
                R.id.currencyListFragment,
                R.id.chartFragment
            )
        )

        /**
         * Situation: Going from CurrencyList to Charts
         * Issue: Cannot navigate back to CurrencyList from Charts
         *        by clicking the bottom nav icon
         * **/

        navView.setupWithNavController(navController)
        setupActionBarWithNavController(navController, appBarConfiguration)

    }
}