package com.example.troopers_currency_converter.ui.fragment.converter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.troopers_currency_converter.R
import com.example.troopers_currency_converter.databinding.FragmentConverterBinding
import com.example.troopers_currency_converter.model.Currency
import com.example.troopers_currency_converter.ui.dialog_fragment.ChooseBaseCurrencyFragment
import com.example.troopers_currency_converter.ui.interfaces.SendDataFromDialogFragment
import com.example.troopers_currency_converter.utils.app.AppUtils
import com.example.troopers_currency_converter.utils.network.resource.NetworkResult
import kotlinx.coroutines.launch
import kotlin.math.roundToInt

class ConverterFragment : Fragment(), SendDataFromDialogFragment {

    private var _binding: FragmentConverterBinding? = null
    private val binding get() = _binding!!

    private lateinit var converterViewModel: ConverterViewModel

    private var fromCurrency: String = "USD"
    private var toCurrency: String = "MYR"
    private var amountToConvert = "100"
    private lateinit var baseCurrency: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        converterViewModel = ViewModelProvider(requireActivity())[ConverterViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentConverterBinding.inflate(inflater, container, false)

        val view = binding.root

        baseCurrency = ""
        viewLifecycleOwner.lifecycleScope.launch {
            converterViewModel.readUserBaseCurrency.collect { values ->
                baseCurrency = values.baseCurrency
            }
        }

        (activity as AppCompatActivity).supportActionBar?.hide()
        binding.homeFragToolbar.toolbarTitle.apply {
            text = getString(R.string.exchange_rate)
            //setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_currency_exchange, 0, 0, 0)
        }

        readCachedUserBaseCurrency()
        setupCurrencySpinner()

        binding.baseCurrencyCV.setOnClickListener {
            showBaseCurrencyChooseDialogFrag()
        }

        binding.convertBtn.setOnClickListener {
            sendConvertApiRequest()
        }

        return view
    }

    private fun readCachedUserBaseCurrency() {
        //baseCurrency = "JPY"
        viewLifecycleOwner.lifecycleScope.launch {
            converterViewModel.readUserBaseCurrency.collect { values ->
                baseCurrency = values.baseCurrency
            }
        }
        setBaseCurrencyView()
    }

    private fun setBaseCurrencyView() {
        // Verify this block here
        // readCachedUserBaseCurrency()
        val baseCurrencyFlag: Int = Currency.valueOf(baseCurrency).flag!!
        binding.chosenBaseCurrencyTv.text = baseCurrency
        binding.baseCurrencyFlagIV.setImageResource(baseCurrencyFlag)
        binding.baseCurrencyDetail.text = getString(R.string.click_to_choose_base)
    }

    private fun setupCurrencySpinner() {
        val spinnerAdapter = ArrayAdapter.createFromResource(
            requireContext(),
            R.array.currency_symbol_adapter,
            android.R.layout.simple_spinner_item
        )

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.apply {
            this.fromCurrencySpinner.adapter = spinnerAdapter
            this.toCurrencySpinner.adapter = spinnerAdapter
        }

        binding.fromCurrencySpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    val fromCurrencySelected = parent!!.getItemAtPosition(position).toString()
                    fromCurrency = fromCurrencySelected
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }
            }

        binding.toCurrencySpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    val toCurrencySelected = parent!!.getItemAtPosition(position).toString()
                    toCurrency = toCurrencySelected
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }
            }
    }

    private fun retrieveUserInput(): Triple<String, String, String> {
        amountToConvert = binding.amountToConvertTil.editText?.text.toString()
        fromCurrency = binding.fromCurrencySpinner.selectedItem.toString().substring(0, 3)
        toCurrency = binding.toCurrencySpinner.selectedItem.toString().substring(0, 3)
        return Triple(toCurrency, fromCurrency, amountToConvert)
    }

    private fun sendConvertApiRequest() {

        val (to, from, amount) = retrieveUserInput()
        converterViewModel.getConvertedAmount(to, from, amount)
        converterViewModel.convertedResponse.observe(viewLifecycleOwner) { response ->

            binding.convertedResultTV.text = ""
            binding.convertedResultDetailsTV.text = ""
            binding.currencyConverterProgressBar.visibility = View.VISIBLE

            when (response) {

                is NetworkResult.Success -> {
                    binding.currencyConverterProgressBar.visibility = View.GONE
                    val convertedResponse = response.data!!.result
                    val toCurrencySymbol = Currency.valueOf(response.data.query.to).symbol
                    val roundedConvertedResponse = (convertedResponse * 100.0).roundToInt() / 100.0

                    val cName = getString(Currency.valueOf(from).fullName)
                    binding.convertedResultDetailsTV.text =
                        getString(R.string.converted_result_details, amount, cName)
                    val convertedResult = "$toCurrencySymbol $roundedConvertedResponse"
                    binding.convertedResultTV.text = convertedResult
                }

                is NetworkResult.Error -> {
                    binding.currencyConverterProgressBar.visibility = View.GONE
                    AppUtils.showToast(requireContext(), response.message.toString())
                }

                is NetworkResult.Loading -> {
                    binding.currencyConverterProgressBar.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun showBaseCurrencyChooseDialogFrag() {
        AppUtils.showDialogFragment(
            ChooseBaseCurrencyFragment.newInstance(),
            childFragmentManager,
            "USER_CHOOSE_BASE_CURRENCY_DF"
        )
    }

    override fun sendData(baseCurrency: String) {
        converterViewModel.saveUserBaseCurrency(baseCurrency)
        readCachedUserBaseCurrency()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}