package com.example.troopers_currency_converter.ui.dialog_fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import com.example.troopers_currency_converter.R
import com.example.troopers_currency_converter.databinding.FragmentChooseBaseCurrencyBinding
import com.example.troopers_currency_converter.ui.interfaces.SendDataFromDialogFragment

class ChooseBaseCurrencyFragment : DialogFragment() {

//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_choose_base_currency, container, false)
//    }

    private var sendDataInterface: SendDataFromDialogFragment? = null
    private var chosenBaseCurrency: String = "SGD"

    private var _binding: FragmentChooseBaseCurrencyBinding? = null
    private val binding get() = _binding!!

    companion object {
        fun newInstance(): ChooseBaseCurrencyFragment = ChooseBaseCurrencyFragment()
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog?.setCancelable(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentChooseBaseCurrencyBinding.inflate(inflater, container, false)
        val view = binding.root

        //setupCurrencySpinner()

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sendDataInterface = parentFragment as SendDataFromDialogFragment

    }

    override fun onDetach() {
        sendDataInterface = null
        super.onDetach()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupCurrencySpinner()

        binding.chooseBaseCurrencyBtn.setOnClickListener {
            sendDataInterface?.sendData(chosenBaseCurrency)
            dismiss()
        }
    }

    private fun setupCurrencySpinner() {
        val spinnerAdapter = ArrayAdapter.createFromResource(
            requireContext(),
            R.array.currency_symbol_adapter,
            android.R.layout.simple_spinner_item
        )

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.apply {
            this.userChooseBaseCurrencySpinner.adapter = spinnerAdapter
        }

        binding.userChooseBaseCurrencySpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    val fromCurrencySelected = parent!!.getItemAtPosition(position).toString().substring(0, 3)
                    chosenBaseCurrency = fromCurrencySelected
                    //sendDataInterface?.sendData(fromCurrencySelected)
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }
            }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}