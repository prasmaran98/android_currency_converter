package com.example.troopers_currency_converter.ui.interfaces

interface SendDataFromDialogFragment {
    fun sendData(baseCurrency: String)
}