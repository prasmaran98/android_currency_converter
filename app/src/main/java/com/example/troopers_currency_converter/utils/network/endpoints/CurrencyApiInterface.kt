package com.example.troopers_currency_converter.utils.network.endpoints

import com.example.troopers_currency_converter.model.network.convert.ConvertAmountFromToResponse
import com.example.troopers_currency_converter.model.network.latest_rates.LatestRateResponse
import com.example.troopers_currency_converter.model.network.timeseries.CurrencySeriesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApiInterface {

    /** Get converted amount **/
    @GET("exchangerates_data/convert")
    suspend fun getConvertedAmount(
        @Query("to") to: String,
        @Query("from") from: String,
        @Query("amount") amount: String
    ): Response<ConvertAmountFromToResponse>

    /** Get latest rate against base currency **/
    @GET("exchangerates_data/latest")
    suspend fun getLatestRate(
        @Query("base") base: String
    ): Response<LatestRateResponse>

    /** Get past 7 days graph **/
    @GET("exchangerates_data/timeseries")
    suspend fun getTimeSeriesChart(
        @Query("start_date") start_date: String,
        @Query("end_date") end_date: String,
        @Query("base") base: String,
        @Query("symbols") symbols: String
    ) : Response<CurrencySeriesResponse>


}