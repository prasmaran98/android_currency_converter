package com.example.troopers_currency_converter.utils.dependencyInjection

import android.content.Context
import androidx.core.content.ContextCompat
import com.example.troopers_currency_converter.R
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Interceptor to add apikey to headers
 */
@Singleton
class AuthInterceptor @Inject constructor(@ApplicationContext private val context: Context) :
    Interceptor {

    private val apiHeader = "apikey"
    private val apiKey = context.getString(R.string.currency_rate_api_key)

    /**
     * Maybe can use data store
     * to store the API Key and retrieve?
     * keeping @param context for that purpose
     **/

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()
        requestBuilder.addHeader(apiHeader, apiKey)
        return chain.proceed(requestBuilder.build())
    }

}