package com.example.troopers_currency_converter.utils.dependencyInjection

import android.content.Context
import androidx.room.Room
import com.example.troopers_currency_converter.utils.constant.Constants
import com.example.troopers_currency_converter.utils.database.CurrencyDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(
        context,
        CurrencyDatabase::class.java,
        Constants.CURRENCY_DATABASE
    ).build()

    @Singleton
    @Provides
    fun provideDao(database: CurrencyDatabase) = database.currencyDao()

}