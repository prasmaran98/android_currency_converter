package com.example.troopers_currency_converter.model.network.timeseries


import android.os.Parcelable
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

@Parcelize
data class CurrencySeriesResponse(
    @SerializedName("base")
    val base: String,
    @SerializedName("end_date")
    val endDate: String,
    @SerializedName("rates")
    val rates: Map<String, Map<String,Double>>,
    @SerializedName("start_date")
    val startDate: String,
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("timeseries")
    val timeseries: Boolean
): Parcelable