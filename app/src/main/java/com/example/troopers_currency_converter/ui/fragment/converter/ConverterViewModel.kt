package com.example.troopers_currency_converter.ui.fragment.converter

import android.app.Application
import androidx.lifecycle.*
import com.example.troopers_currency_converter.model.network.convert.ConvertAmountFromToResponse
import com.example.troopers_currency_converter.utils.app.DataStoreRepository
import com.example.troopers_currency_converter.utils.network.resource.NetworkResult
import com.example.troopers_currency_converter.utils.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ConverterViewModel @Inject constructor(
    application: Application,
    private val repository: Repository,
    private val dataStoreRepository: DataStoreRepository
) : AndroidViewModel(application) {

    //TODO: Network Listener
    var networkStatus: Boolean = false
    var backOnline: Boolean = false

    val readUserBaseCurrency = dataStoreRepository.readUserBaseCurrency

    private val _convertedResponse = MutableLiveData<NetworkResult<ConvertAmountFromToResponse>>()
    var convertedResponse: LiveData<NetworkResult<ConvertAmountFromToResponse>> = _convertedResponse

    fun getConvertedAmount(
        to: String,
        from: String,
        amount: String
    ) = viewModelScope.launch {
        getConvertedAmountSafeCall(to,from,amount)
    }

    private suspend fun getConvertedAmountSafeCall(
        to: String,
        from: String,
        amount: String
    ) {
        _convertedResponse.value = NetworkResult.Loading()

        try {
            val response = repository.remote.getConvertedAmount(to,from,amount)
            _convertedResponse.value = handleGetConvertedAmountResponse(response)
            //TODO: Offline caching

        } catch (e: Exception) {
            _convertedResponse.value = NetworkResult.Error(e.message.toString())
            println("Error: ${e.message.toString()}")
        }
    }

    private fun handleGetConvertedAmountResponse(response: Response<ConvertAmountFromToResponse>): NetworkResult<ConvertAmountFromToResponse>? {

        return when {
            !response.body()!!.success -> NetworkResult.Error("An error occurred")
            response.isSuccessful -> {
                val data = response.body()
                NetworkResult.Success(data!!)
            }
            else -> {
                NetworkResult.Error("An error has occurred. Please try again")
            }
        }
    }

    // Save user base currency choice
    fun saveUserBaseCurrency(baseCurrency: String) {
        viewModelScope.launch(Dispatchers.IO) {
            dataStoreRepository.saveUserBaseCurrency(baseCurrency)
        }
    }

}