package com.example.troopers_currency_converter.utils.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface CurrencyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCurrencyRate(currencyEntity: CurrencyEntity)

    @Query("SELECT * FROM currencyRateDatabase ORDER BY id ASC")
    fun readDatabase(): Flow<List<CurrencyEntity>>

}