package com.example.troopers_currency_converter.model.network.convert

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ConvertAmountFromToResponse(
    val date: String,
    val info: Info,
    val query: Query,
    val result: Double,
    val success: Boolean
) : Parcelable