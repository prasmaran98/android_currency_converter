package com.example.troopers_currency_converter.utils.database

import androidx.room.TypeConverter
import com.example.troopers_currency_converter.model.network.latest_rates.LatestRateResponse
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class CurrencyTypeConverter {

    /***
     * It seems I need type converters
     * to parse data model into Room Database
     * Need to look into this deeper soon
     * */

    var gson = Gson()

    @TypeConverter
    fun currencyRateToString(currencyRate: LatestRateResponse): String{
        return gson.toJson(currencyRate)
    }

    @TypeConverter
    fun stringToCurrencyRate(data: String): LatestRateResponse{
        val listType = object : TypeToken<LatestRateResponse>() {}.type
        return gson.fromJson(data, listType)
    }

}