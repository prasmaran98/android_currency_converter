package com.example.troopers_currency_converter.ui.fragment.chart

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.example.troopers_currency_converter.R
import com.example.troopers_currency_converter.databinding.FragmentChartBinding
import com.example.troopers_currency_converter.utils.app.AppUtils
import com.example.troopers_currency_converter.utils.network.resource.NetworkResult
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import kotlinx.coroutines.launch

class ChartFragment : Fragment() {

    private val binding get() = _binding!!
    private var _binding: FragmentChartBinding? = null
    private val args by navArgs<ChartFragmentArgs>()
    private lateinit var chartViewModel: ChartViewModel

    private val defaultBaseCurrency = "USD"
    private val defaultToCurrency = "MYR"
    private var cachedBaseCurrency: String? = null
    private var clickedToCurrency: String? = null

    private var receivedChartData: MutableMap<String, Double> = mutableMapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        chartViewModel = ViewModelProvider(requireActivity())[ChartViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentChartBinding.inflate(inflater, container, false)
        val view = binding.root

        (activity as AppCompatActivity).supportActionBar?.hide()
        binding.homeFragToolbar.toolbarTitle.text = getString(R.string.historical_data)

        viewLifecycleOwner.lifecycleScope.launch {
            chartViewModel.readUserBaseCurrency.collect { values ->
                cachedBaseCurrency = values.baseCurrency
            }
        }

        clickedToCurrency = args.chartData?.symbol ?: defaultToCurrency

        sendTimeSeriesChartApiRequest()

        return view
    }

    private fun retrieveInputForApi(): Array<String> {
        val (today, past) = AppUtils.getCurrentAndPastDateForOneWeek()
        val base = cachedBaseCurrency ?: defaultBaseCurrency
        val symbol = clickedToCurrency ?: defaultToCurrency
        return arrayOf(past, today, base, symbol)
    }

    private fun sendTimeSeriesChartApiRequest() {

        val (from, today, base, symbol) = retrieveInputForApi()
        chartViewModel.getTimeSeriesChart(from, today, base, symbol)
        chartViewModel.timeSeriesResponse.observe(viewLifecycleOwner) { response ->

            when (response) {

                is NetworkResult.Success -> {
                    binding.chartProgressBar.visibility = View.GONE
                    //val receivedBaseCurrency = response.data?.base
                    val responseData = response.data!!.rates

                    responseData.forEach { (s, map) ->
                        map.forEach { (_, d) ->
                            receivedChartData[s] = d
                        }
                    }
                    Log.i("CHART", "sendTimeSeriesChartApiRequest: ")
                    setData(receivedChartData)

                    AppUtils.showToast(requireContext(), "Successfully retrieved")
                    println(responseData.toString())
                }

                is NetworkResult.Error -> {
                    binding.chartProgressBar.visibility = View.GONE
                    AppUtils.showToast(requireContext(), response.message.toString())
                }

                is NetworkResult.Loading -> {

                }
            }
        }
    }

    private fun setData(rates: Map<String, Double>) {
        binding.chartProgressBar.visibility = View.GONE
        binding.chartFragTitleTv.text = "$clickedToCurrency against $cachedBaseCurrency"
        val xValue = AppUtils.getCurrentAndPastDateFormattedForChart()
        val lineEntry = ArrayList<Entry>()

        var index = 0
        rates.forEach { (_, d) ->
            lineEntry.add(Entry((index + 1).toFloat(), d.toFloat()))
            index++
        }

        println(xValue.toString())
        val lineDataSet = LineDataSet(lineEntry, "$clickedToCurrency against $cachedBaseCurrency")

        lineDataSet.apply {
            color = ContextCompat.getColor(requireContext(), R.color.darkPink)
            lineWidth = 2f
            isHighlightEnabled = true
            setDrawHighlightIndicators(false)
            mode = LineDataSet.Mode.LINEAR
        }

        val data = LineData(lineDataSet)

        binding.lineChart.apply {
            this.data = data
            this.animateXY(2000, 2000)
            setTouchEnabled(true)
            isDragEnabled = true
            setPinchZoom(true)
            description.isEnabled = false
            setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.lightGray))

            xAxis.apply {
                isGranularityEnabled = true
                granularity = 1f
                position = XAxis.XAxisPosition.BOTTOM
                valueFormatter = IndexAxisValueFormatter(xValue)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}