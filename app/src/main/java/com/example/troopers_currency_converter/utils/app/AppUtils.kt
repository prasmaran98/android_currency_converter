package com.example.troopers_currency_converter.utils.app

import android.app.Application
import android.content.Context
import android.os.Build
import android.provider.Settings.System.getString
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.troopers_currency_converter.model.Currency
import com.google.android.material.snackbar.Snackbar
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class AppUtils {

    companion object {

        /** Toast message function **/
        fun showToast(context: Context, msg: String) {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
        }

        /** Snack bar message function **/
        fun View.showSnackBar(msg: String) {
            Snackbar.make(this, msg, Snackbar.LENGTH_LONG).also { snackbBar ->
                snackbBar.setAction("OKAY") {
                    snackbBar.dismiss()
                }
            }.show()
        }

        /** Custom dialog fragment function **/
        fun showDialogFragment(
            dialogFragment: DialogFragment,
            fragmentManager: FragmentManager,
            fragTag: String
        ) {
            val ft: FragmentTransaction = fragmentManager.beginTransaction()
            val prev: Fragment? =
                fragmentManager.findFragmentByTag(fragTag)
            if (prev != null) {
                ft.remove(prev)
            }
            ft.addToBackStack(null)
            dialogFragment.show(ft, fragTag)
        }

        @RequiresApi(Build.VERSION_CODES.O)
        fun getCurrentAndPastDateForOneWeek(): Pair<String, String>{
            val current = LocalDateTime.now()
            val past = current.minusDays(7)
            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            return Pair(current.format(formatter),past.format(formatter))
        }

        @RequiresApi(Build.VERSION_CODES.O)
        fun getCurrentAndPastDateFormattedForChart(): List<String>{
            val formattedDate = mutableListOf<String>()
            for (i in 0..6) {
                val current = LocalDateTime.now()
                val past = current.minusDays(i.toLong())
                val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
                val formatted = past.format(formatter)
                formattedDate.add(formatted.split(",")[0].dropLast(4).trim())
            }
            return formattedDate.reversed()
        }

        fun getCurrencyShortAndFullName(context: Context) : List<String> {
            val currencyForSpinner = mutableListOf<String>()
            enumValues<Currency>().forEach { currency ->
                currencyForSpinner.add("${currency.short} : ${context.getString(currency.fullName)}")
            }
            return currencyForSpinner
        }

    }

}