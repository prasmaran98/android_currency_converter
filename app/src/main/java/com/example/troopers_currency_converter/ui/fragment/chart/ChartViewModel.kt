package com.example.troopers_currency_converter.ui.fragment.chart

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.troopers_currency_converter.model.network.timeseries.CurrencySeriesResponse
import com.example.troopers_currency_converter.utils.app.DataStoreRepository
import com.example.troopers_currency_converter.utils.network.resource.NetworkResult
import com.example.troopers_currency_converter.utils.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ChartViewModel @Inject constructor(
    application: Application,
    private val repository: Repository,
    private val dataStoreRepository: DataStoreRepository
) : AndroidViewModel(application) {

    //TODO: Network Listener
    var networkStatus: Boolean = false
    var backOnline: Boolean = false

    private val _timeSeriesResponse = MutableLiveData<NetworkResult<CurrencySeriesResponse>>()
    val timeSeriesResponse: LiveData<NetworkResult<CurrencySeriesResponse>> = _timeSeriesResponse

    val readUserBaseCurrency = dataStoreRepository.readUserBaseCurrency

    fun getTimeSeriesChart(
        start_date: String,
        end_date: String,
        base: String,
        symbols: String
    ) = viewModelScope.launch {
        getTimeSeriesChartSafeCall(start_date, end_date, base, symbols)
    }

    private suspend fun getTimeSeriesChartSafeCall(
        start_date: String,
        end_date: String,
        base: String,
        symbols: String
    ) {
        _timeSeriesResponse.value = NetworkResult.Loading()

        try {
            val response = repository.remote.getTimeSeriesChart(start_date,end_date,base,symbols)
            _timeSeriesResponse.value = handleGetTimeSeriesChartResponse(response)

            //TODO: Offline caching

        } catch (e: Exception) {
            _timeSeriesResponse.value = NetworkResult.Error(e.message.toString())
            println("Error: ${e.message.toString()}")
        }
    }

    private fun handleGetTimeSeriesChartResponse(response: Response<CurrencySeriesResponse>): NetworkResult<CurrencySeriesResponse> {

        return when {
            !response.body()!!.success -> NetworkResult.Error("An error occurred")
            response.isSuccessful -> {
                val data = response.body()
                NetworkResult.Success(data!!)
            }
            else -> {
                NetworkResult.Error(response.message())
            }
        }
    }




}