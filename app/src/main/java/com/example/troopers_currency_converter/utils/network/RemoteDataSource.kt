package com.example.troopers_currency_converter.utils.network

import com.example.troopers_currency_converter.model.network.convert.ConvertAmountFromToResponse
import com.example.troopers_currency_converter.model.network.latest_rates.LatestRateResponse
import com.example.troopers_currency_converter.model.network.timeseries.CurrencySeriesResponse
import com.example.troopers_currency_converter.utils.network.endpoints.CurrencyApiInterface
import retrofit2.Response
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val currencyApiInterface: CurrencyApiInterface
) {

    suspend fun getConvertedAmount(
        to: String,
        from: String,
        amount: String
    ): Response<ConvertAmountFromToResponse> {
        return currencyApiInterface.getConvertedAmount(to,from,amount)
    }

    suspend fun getLatestRate(
        base: String
    ): Response<LatestRateResponse> {
        return currencyApiInterface.getLatestRate(base)
    }

    suspend fun getTimeSeriesChart(
        start_date: String,
        end_date: String,
        base: String,
        symbols: String
    ) : Response<CurrencySeriesResponse> {
        return currencyApiInterface.getTimeSeriesChart(start_date, end_date, base, symbols)
    }

}