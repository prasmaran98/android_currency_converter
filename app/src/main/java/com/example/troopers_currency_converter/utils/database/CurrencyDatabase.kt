package com.example.troopers_currency_converter.utils.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters


@Database(
    version = 1,
    entities = [CurrencyEntity::class],
    exportSchema = false
)
@TypeConverters(CurrencyTypeConverter::class)
abstract class CurrencyDatabase: RoomDatabase() {

    abstract fun currencyDao(): CurrencyDao

}