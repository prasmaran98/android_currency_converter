package com.example.troopers_currency_converter.ui.base

import androidx.appcompat.app.AppCompatActivity

open class BaseActivity: AppCompatActivity()