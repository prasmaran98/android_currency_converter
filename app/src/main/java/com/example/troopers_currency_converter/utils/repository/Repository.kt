package com.example.troopers_currency_converter.utils.repository

import com.example.troopers_currency_converter.utils.network.LocalDataSource
import com.example.troopers_currency_converter.utils.network.RemoteDataSource
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class Repository @Inject constructor(
    remoteDataSource: RemoteDataSource,
    localDataSource: LocalDataSource
) {

    val remote = remoteDataSource
    val local = localDataSource

}