package com.example.troopers_currency_converter.utils.adapter.rvAdapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.surgeryapptest.utils.diffUtils.CustomDiffUtils
import com.example.troopers_currency_converter.databinding.CurrencyRateRowLayoutBinding
import com.example.troopers_currency_converter.model.Currency
import com.example.troopers_currency_converter.model.network.CurrencyRate

class CurrencyListAdapter : RecyclerView.Adapter<CurrencyListAdapter.MyViewHolder>() {

    private var currencyList = emptyList<CurrencyRate>()

    class MyViewHolder(private val binding: CurrencyRateRowLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(currencyDataItem: CurrencyRate){
            binding.currencyListResult = currencyDataItem
            val fullDetails = "${currencyDataItem.symbol} -- ${itemView.context.getString(Currency.valueOf(currencyDataItem.symbol).fullName)}"
            binding.fullName = fullDetails
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup) : MyViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = CurrencyRateRowLayoutBinding.inflate(layoutInflater, parent, false)
                return MyViewHolder(binding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyListAdapter.MyViewHolder {
        return MyViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: CurrencyListAdapter.MyViewHolder, position: Int) {
        val currentCurrencyItem = currencyList[position]
        holder.bind(currentCurrencyItem)
    }

    override fun getItemCount(): Int = currencyList.size

    fun setData(newData : List<CurrencyRate>){
        val diffUtils = CustomDiffUtils(currencyList, newData)
        val diffUtilsResult = DiffUtil.calculateDiff(diffUtils)
        currencyList = newData
        diffUtilsResult.dispatchUpdatesTo(this)
    }

//    inner class CurrencyListItemViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
//        LayoutInflater.from(parent.context).inflate(R.layout.currency_rate_row_layout, parent, false)
//    ) {
//
//        private val binding = CurrencyRateRowLayoutBinding.bind(itemView)
//
//        fun onBind(currencyDataItem: CurrencyRate) {
//
//            val currencyShort =  Currency.valueOf(currencyDataItem.symbol).short
//            val currencyFlag: Int = Currency.valueOf(currencyShort).flag!!
//            val currencySign = Currency.valueOf(currencyDataItem.symbol).symbol // Currency Rate symbol refers to short hand, mistakenly named the data class val
//            val currencyFullName = Currency.valueOf(currencyDataItem.symbol).fullName
//
//            binding.currencyFlagImage.setImageResource(currencyFlag)
//            val currencyRateToBind = "$currencySign ${currencyDataItem.rate.roundToInt()}"
//            val currencyDetailsToBind = "${currencyDataItem.symbol} -- $currencyFullName"
//            binding.rateTv = currencyRateToBind
//            binding.currencyFullNameTv = currencyDetailsToBind
//
//        }
//
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
//        return CurrencyListItemViewHolder(parent)
//    }
//
//    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        (holder as CurrencyListItemViewHolder).onBind(currencyList[position]!!)
//    }
//
//    override fun getItemCount(): Int {
//        return currencyList.size
//    }
//
//    @SuppressLint("NotifyDataSetChanged")
//    fun setItems(currencyListItems: List<CurrencyRate>) {
//        this.currencyList.clear()
//        this.currencyList.addAll(currencyListItems)
//        notifyDataSetChanged()
//    }
}

