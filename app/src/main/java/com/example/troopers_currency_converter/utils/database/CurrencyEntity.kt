package com.example.troopers_currency_converter.utils.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.troopers_currency_converter.model.network.latest_rates.LatestRateResponse
import com.example.troopers_currency_converter.utils.constant.Constants.Companion.CURRENCY_DATABASE

@Entity(tableName = CURRENCY_DATABASE)
class CurrencyEntity(
    var currencyRate: LatestRateResponse
) {
    @PrimaryKey(autoGenerate = false)
    var id: Int = 0
}