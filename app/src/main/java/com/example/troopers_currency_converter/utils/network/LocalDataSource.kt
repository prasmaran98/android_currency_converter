package com.example.troopers_currency_converter.utils.network

import androidx.lifecycle.LiveData
import com.example.troopers_currency_converter.utils.database.CurrencyDao
import com.example.troopers_currency_converter.utils.database.CurrencyEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LocalDataSource @Inject constructor(
    private val currencyDao: CurrencyDao
){

    fun readDataBase(): Flow<List<CurrencyEntity>> {
        return currencyDao.readDatabase()
    }

    suspend fun insertCurrencyRate(currencyEntity: CurrencyEntity) {
        currencyDao.insertCurrencyRate(currencyEntity)
    }

}